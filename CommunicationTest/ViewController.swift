//
//  ViewController.swift
//  CommunicationTest
//
//  Created by Parrot on 2019-10-26.
//  Copyright © 2019 Parrot. All rights reserved.
//

import UIKit
import WatchConnectivity

class ViewController: UIViewController, WCSessionDelegate  {

    // MARK: Outlets
    @IBOutlet weak var outputLabel: UITextView!
    
    var pokemonName = ""
    var pokemonType = ""
    var health = 0
    var hunger = 0
    
    
    // MARK: Required WCSessionDelegate variables
    // ------------------------------------------
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        //@TODO
    }
    
    func sessionDidBecomeInactive(_ session: WCSession) {
        //@TODO
    }
    
    func sessionDidDeactivate(_ session: WCSession) {
        //@TODO
    }
    
    // MARK: Receive messages from Watch
    // -----------------------------------
    
    // 3. This function is called when Phone receives message from Watch
    func session(_ session: WCSession, didReceiveMessage message: [String : Any], replyHandler: @escaping ([String : Any]) -> Void) {
        
        // 1. When a message is received from the watch, output a message to the UI
        // NOTE: Since session() runs in background, you cannot directly update UI from the background thread.
        // Therefore, you need to wrap any UI updates inside a DispatchQueue for it to work properly.
        DispatchQueue.main.async {
            
            
            var pokemonName = message["pokemonName"] as! String
            var pokemonType = message["pokemonType"] as! String
            var health = message["health"] as! String
            var hunger = message["hunger"] as! String
            
            self.pokemonName = pokemonName
            self.pokemonType = pokemonType
            self.health = Int(health)!
            self.hunger = Int(hunger)!
            
            print("defaults value print and save")
            // saving in the user defaults
            let defaults = UserDefaults.standard
            //Set
            defaults.set(pokemonName, forKey: "pokemonName")
            defaults.set(pokemonType, forKey: "pokemonType")
            defaults.set(health, forKey: "health")
            defaults.set(hunger, forKey: "hunger")
            
            print("defaults value")
            print(defaults.string(forKey: "health"))
            
           
           
        
            self.outputLabel.insertText("\nMessage Received: \(pokemonName, pokemonType, health, hunger)")
            
             self.outputLabel.insertText("\nMessage Received: Pokemon is hybernating wake up)")
        }
        
        // 2. Also, print a debug message to the phone console
        // To make the debug message appear, see Moodle instructions
        print("Received a message from the watch: \(message)")
    }

    
    // MARK: Default ViewController functions
    // -----------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        // 1. Check if phone supports WCSessions
        print("view loaded")
        if WCSession.isSupported() {
            outputLabel.insertText("\nPhone supports WCSession")
            WCSession.default.delegate = self
            WCSession.default.activate()
            outputLabel.insertText("\nSession activated")
        }
        else {
            print("Phone does not support WCSession")
            outputLabel.insertText("\nPhone does not support WCSession")
        }
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    // MARK: Actions
    // -------------------
    @IBAction func sendMessageButtonPressed(_ sender: Any) {
        
        // 2. When person presses button, send message to watch
        outputLabel.insertText("\nTrying to send message to watch1")
        
        if WCSession.default.isReachable {
            let message = ["pokemonName" : "","pokemonType" : "", "health" : "\(self.health)", "hunger" : "\(self.hunger)"]
            WCSession.default.sendMessage(message, replyHandler: nil)
            // output a debug message to the UI
            outputLabel.insertText("\nmessage sent to watch")
            // output a debug message to the console
            print("Message sent to watch")
          
        }
        else {
            print("Phone is not reachable")
           
        }
        
    }
    
    
    // MARK: Choose a Pokemon actions
    
    @IBAction func pokemonButtonPressed(_ sender: Any) {
        print("You pressed the pokemon button")
        // 1. Try to send a message to the phone
        if (WCSession.default.isReachable) {
            let message = ["pokemonSelected": "pikachu"]
            WCSession.default.sendMessage(message, replyHandler: nil)
            // output a debug message to the UI
            outputLabel.insertText("\nYou selected pokemon")
            // output a debug message to the console
            print("Message sent to watch")
        }
        else {
            print("PHONE: Cannot reach watch")
            outputLabel.insertText("\nCannot reach watch")
        }
    }
    @IBAction func caterpieButtonPressed(_ sender: Any) {
        print("You pressed the caterpie button")
        // 1. Try to send a message to the phone
        if (WCSession.default.isReachable) {
            let message = ["pokemonSelected": "caterpie"]
            WCSession.default.sendMessage(message, replyHandler: nil)
            // output a debug message to the UI
            outputLabel.insertText("\nYou selected pokemon")
            // output a debug message to the console
            print("Message sent to watch")
        }
        else {
            print("PHONE: Cannot reach watch")
            outputLabel.insertText("\nCannot reach watch")
        }
    }
    
    
}

