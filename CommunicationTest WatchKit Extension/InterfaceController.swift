//
//  InterfaceController.swift
//  CommunicationTest WatchKit Extension
//
//  Created by Parrot on 2019-10-26.
//  Copyright © 2019 Parrot. All rights reserved.
//

import WatchKit
import Foundation
import WatchConnectivity

class InterfaceController: WKInterfaceController, WCSessionDelegate {
    var sampleClassObject : Screen2Sample = Screen2Sample()
    
    var flagHibernate = false

    
    // MARK: Outlets
    // ---------------------
    @IBOutlet var messageLabel: WKInterfaceLabel!
   
    // Imageview for the pokemon
    @IBOutlet var pokemonImageView: WKInterfaceImage!
    // Label for Pokemon name (Albert is hungry)
    @IBOutlet var nameLabel: WKInterfaceLabel!
    // Label for other messages (HP:100, Hunger:0)
    @IBOutlet var outputLabel: WKInterfaceLabel!
    var hunger = 0
    var health = 100
    
    // setting up timer
    //var counter = 0
    var counterTimer = Timer()
    var counterStartValue = 0
    
    func startCounter(){
        
       
        counterTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(incrementCounter), userInfo: nil, repeats: true)
        
        
    }
    @objc func incrementCounter(){
       
        
        if(self.flagHibernate == false){
       
        counterStartValue = counterStartValue + 1
         self.messageLabel.setText("Pokemon is \(counterStartValue) sec old")
        if(self.health > 0 && self.health <= 100){
        
        if(counterStartValue % 5 == 0 && self.hunger < 80){
            self.hunger = self.hunger + 10
            self.outputLabel.setText("HP: 100  Hunger:\(self.hunger)")
        }
        else if(counterStartValue % 5 == 0 && self.hunger >= 80){
            self.hunger = self.hunger + 10
            
            
            self.health = self.health - 5
            self.outputLabel.setText("HP: \(self.health)  Hunger:\(self.hunger)")
        }
        }
        
        if(self.health <= 0){
            self.messageLabel.setText("Pokemon died")
            self.outputLabel.setText("HP: \(self.health)  Hunger:\(self.hunger)")
        }
        }
        
        else{
             self.messageLabel.setText("Pokemon is \(counterStartValue) sec old")
            self.outputLabel.setText("HP: \(self.health)  Hunger:\(self.hunger)")
        }
    }
    
    // MARK: Delegate functions
    // ---------------------

    // Default function, required by the WCSessionDelegate on the Watch side
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        //@TODO
        
    }
    
    // 3. Get messages from PHONE
    func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
        print("WATCH: Got message from Phone about pet hibernate details")
        // Message from phone comes in this format: ["course":"MADT"]
        DispatchQueue.main.async {
            
            
            var pokemonName = message["pokemonName"] as! String
            var pokemonType = message["pokemonType"] as! String
            var health = message["health"] as! String
            var hunger = message["hunger"] as! String
            
//            self.pokemonName = pokemonName
//            self.pokemonType = pokemonType
//            self.health = Int(health)!
//            self.hunger = Int(hunger)!
            print(hunger)
            self.resetDetails(health: health, hunger: hunger)
            
           
        }
        
        // 2. Also, print a debug message to the phone console
        // To make the debug message appear, see Moodle instructions
        print("Received a message from the watch: \(message)")
    }
    func resetDetails(health: String, hunger: String){
        
        self.flagHibernate = false
        
        let defaults = UserDefaults.standard
       
        print(defaults.string(forKey: "health"))
        
       // self.nameLabel.setText("\(defaults.string(forKey: "pokemonName")) is hungry")
        
        
        self.health = Int(health) ?? 100
        self.hunger = Int(hunger) ?? 0
        self.outputLabel.setText("HP: \(self.health)  Hunger:\(self.hunger)")
    }
    


    
    // MARK: WatchKit Interface Controller Functions
    // ----------------------------------
    override func awake(withContext context: Any?) {
//        super.awake(withContext: context)
       
        
        
        // 1. Check if teh watch supports sessions
        if WCSession.isSupported() {
            WCSession.default.delegate = self
            WCSession.default.activate()
            
//            let pokemonName = self.sampleClassObject.pokemonName
//            self.nameLabel.setText("\(pokemonName)ok")
//            print("okkkk \(pokemonName)")
        }
        
        
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
       
        self.startCounter()
        
        
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

    
    // MARK: Actions
    // ---------------------
    
    // 2. When person presses button on watch, send a message to the phone
    @IBAction func buttonPressed() {
        
      
    }
    
    
    // MARK: Functions for Pokemon Parenting

    
    @IBAction func feedButtonPressed() {
        print("Feed button pressed")
        if(self.hunger - 12 >= 0){
        self.hunger = self.hunger - 12
        self.outputLabel.setText("HP: \(self.health)  Hunger:\(self.hunger)")
        }
        else{
            self.hunger = 0
            self.outputLabel.setText("HP: \(self.health)  Hunger:\(self.hunger)")
        }
    }
    
    @IBAction func hibernateButtonPressed() {
        print("Hibernate button pressed")
        if WCSession.default.isReachable {
           self.flagHibernate = true
            print("Attempting to send message to phone")
            self.messageLabel.setText("Sending msg to watch")
            WCSession.default.sendMessage(
                ["pokemonName" : "albert","pokemonType" : "pikachu", "health" : "\(self.health)", "hunger" : "\(self.hunger)"],
                replyHandler: {
                    (_ replyMessage: [String: Any]) in
                    // @TODO: Put some stuff in here to handle any responses from the PHONE
                    print("Message sent, put something here if u are expecting a reply from the phone")
                    self.messageLabel.setText("Got reply from phone")
            }, errorHandler: { (error) in
                //@TODO: What do if you get an error
                print("Error while sending message: \(error)")
                self.messageLabel.setText("Error sending message")
            })
        }
        else {
            print("Phone is not reachable")
            self.messageLabel.setText("Cannot reach phone")
        }
        
    }
    
}
